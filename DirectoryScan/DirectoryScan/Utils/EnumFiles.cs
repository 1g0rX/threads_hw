﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DirectoryScan.Utils
{
    static class FileInformation
    {
       public static FileInfo[] Get(DirectoryInfo root)
        {
            FileInfo[] files = null;
            List<FileInfo> fileInfos = new List<FileInfo>();

            try
            {
                files = root.GetFiles();
            }
            catch (UnauthorizedAccessException)
            {
               
            }
            catch (DirectoryNotFoundException)
            {
              
            }

            if (files != null)
            {
                fileInfos.AddRange(files);

                DirectoryInfo[] subDirs = root.GetDirectories();
                foreach (DirectoryInfo dirInfo in subDirs)
                {
                    fileInfos.AddRange(Get(dirInfo));
                }
            }

            return fileInfos.ToArray();
        }

        public static IEnumerable<FileInfo> GetEnum(DirectoryInfo root)
        {
            IEnumerable<FileInfo> files;
            try
            {
                files = root.EnumerateFiles();
            }
            catch (UnauthorizedAccessException)
            {
                yield break;
            }
            catch (DirectoryNotFoundException)
            {
                yield break;
            }

            if (files != null)
            {
                foreach (var fi in files)
                {
                    yield return fi;
                }

                DirectoryInfo[] subDirs = root.GetDirectories();
                foreach (DirectoryInfo dirInfo in subDirs)
                {
                    foreach (var fi in GetEnum(dirInfo))
                    {
                        yield return fi;
                    }
                }
            }
        }
    }
}
