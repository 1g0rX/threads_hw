﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Concurrent;
using Utils;
using DirectoryScan.Model;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Windows.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Collections;

namespace DirectoryScan.ViewModel
{
    class MainViewModel : PropertyChangedBase, IDataErrorInfo
    {
        #region Properties
        //Запущен ли процесс подсчёта
        private bool isStartCalc;
        public bool IsStartCalc
        {
            get { return isStartCalc; }
            set { SetField(ref isStartCalc, value); }
        }

        //Текущий обрабатываемый файл
        private string currentFile;
        public string CurrentFile
        {
            get { return currentFile; }
            set { SetField(ref currentFile, value); }
        }

        //Размер папки
        private long directorySize;
        public long DirectorySize
        {
            get { return directorySize; }
            set { SetField(ref directorySize, value); }
        }

        //Путь к начальной папке
        private string directoryPath = null;
        public string DirectoryPath
        {
            get { return directoryPath; }
            set
            {
                SetField(ref directoryPath, value);
            }
        }

        //Список файлов и их размер в байтах
        private ObservableCollection<FInfo> fInfos = new ObservableCollection<FInfo>();
        public ObservableCollection<FInfo> FInfos
        {
            get { return fInfos; }
            set { SetField(ref fInfos, value); }
        }
        #endregion

        #region Fields
        CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
        private readonly object _collectionOfObjectsSync = new object();
        #endregion

        public MainViewModel()
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
            { BindingOperations.EnableCollectionSynchronization(fInfos, _collectionOfObjectsSync); }));
         
        }

        #region Commands
        //Реализует диалог выбора файла
        private RelayCommand openDirDialog;
        public RelayCommand OpenDirDialog =>
            openDirDialog ?? (openDirDialog = new RelayCommand(obj =>
                {
                    FolderBrowserDialog dialog = new FolderBrowserDialog();
                    dialog.Description = "Выберите папку";
                    dialog.SelectedPath = DirectoryPath;
                    DialogResult result = dialog.ShowDialog();
                    if (result == DialogResult.OK) DirectoryPath = dialog.SelectedPath;
                }));

        //Подсчёт размера папки
        private RelayCommand calcDirectory;
        public RelayCommand CalcDirectory =>
            calcDirectory ?? (calcDirectory = new RelayCommand(async obj =>
            {
                if (string.IsNullOrWhiteSpace(DirectoryPath)) return;
                //Если подсчёт запущен - прерываем
                if (isStartCalc)
                {
                    cancelTokenSource.Cancel();
                    return;
                }
                //Инициализация начальных значений переменных
                cancelTokenSource = new CancellationTokenSource();
                IsStartCalc = true;
                DirectorySize = 0;
                CurrentFile = String.Empty;
                FInfos.Clear();

                CancellationToken token = cancelTokenSource.Token;              
                await Task.Factory.StartNew(() => CDir(DirectoryPath, token));

                IsStartCalc = false;
            }));
        #endregion

        #region Validation
        public string Error => null;

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "DirectoryPath":
                        if (string.IsNullOrWhiteSpace(directoryPath))
                            return "Путь не может иметь пустое значение";
                        break;
                }
                return string.Empty;
            }
        }
        #endregion

        #region Methods
        private void CDir(string path, CancellationToken token)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            var files = Utils.FileInformation.GetEnum(dirInfo);

            if (files is null) return;

            foreach (var fi in files)
            {
                if (token.IsCancellationRequested) break;

                lock (_collectionOfObjectsSync)
                {
                    CurrentFile = fi.FullName;
                    FInfos.Add(new FInfo { Path = fi.FullName, Size = fi.Length });
                    DirectorySize += fi.Length;
                }
            }
        }
        #endregion

    }
}

